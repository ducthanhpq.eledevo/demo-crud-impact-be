const mongoose = require("mongoose")
const moment = require("moment")

const CodeSchema = new mongoose.Schema({
    name : {
        type: String,
        require:true,
    },
    date : {
        type: String,
        require:true,
    },
    password : {
        type: String,
        require:true,
    },
    isActive:{
        type: Boolean,
        default: true,
    },
    time : { type : Date, default: moment() }

})

module.exports = mongoose.model("code",CodeSchema)