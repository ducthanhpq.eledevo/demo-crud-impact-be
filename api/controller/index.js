var coder = require('../model/index')
async function getCode(req, res){
    const getAllUser = await coder.find({})
    const remmember = getAllUser.sort((firstUser,secondUser)=>{
    })
    res.send({"result": remmember})
}
async function addCode(req, res){
    const data = {name:req.body.name,date:req.body.date, password : "admin"}
    let addCode = await coder.create(data)
    let totalRecord = await coder.countDocuments({});
    res.send({"addCode":addCode,totalRecord})
}
async function deleteCode(req, res){
    let id = req.params.id
    const deleteCode = await coder.findByIdAndDelete(id)
    let totalRecord = await coder.countDocuments({});
    res.send({"deleteCode":deleteCode,totalRecord})
}
async function updateCode(req, res){
    let id = req.params.id
    let body = req.body
    const updateCode = await coder.findByIdAndUpdate(id,body)
    res.send({"updateCode":updateCode})
}
async function searchCode(req, res){
    let page = parseInt(req.query.page)
    let limit = parseInt(req.query.limit)
    let textSearch = req.query.textSearch
    let skip = (page-1)*limit
    let totalElement = await coder.countDocuments({name : {$regex:textSearch,$options:"i"}})
    let totalPage = Math.ceil (totalElement/limit)
    const searchCode = await coder.find({name : {$regex:textSearch,$options:"i"}}).limit(limit).skip(skip)
    res.send({"result":searchCode,totalPage,totalElement})
}
async function pagination(req,res){
    let page = parseInt(req.query.page)
    let limit = parseInt(req.query.limit)
    let skip = (page-1)*limit
    let totalRecord = await coder.find({})
    let totalElement= totalRecord.length
    let totalPage = Math.ceil (totalRecord.length/limit)
    const remmember = await coder.find({})
    const pagination = remmember.sort((firstUser,secondUser)=>{
        if(secondUser.time - firstUser.time){
            return -1
        }
    }).slice(
        (page - 1)* limit,
        (page - 1)* limit + limit
    )
    res.send({"result":pagination,totalElement ,totalPage})
}
module.exports ={
    getCode,
    addCode,
    deleteCode,
    updateCode,
    searchCode,
    pagination,
}